# Photobox

This software provides a dynamic web page listing the images contained within a
configurable directory and its subdirectories with automatic preview (thumbnail)
generation and download functionality.

## Requirements
This software is written in Python. It can run on any system where Python 3 is
installed. For convenience the package management tool `pip` is recommended. You
can download an archive of the repository or clone it via git, in which case git
is needed as well.

## Installation
To install this software, clone or download this repository and navigate to it.
Then install the Python packages listed in `requirements.txt`.
```bash
git clone https://gitlab.com/koefmatt/photobox.git
cd photobox/
pip3 install -r requirements.txt
```

## Running and configuring the software
To run the software, start the application using the Flask development server
inside the root project folder. Configuration is done via environment variables:
  - **PRODUCT_NAME** Branding used throughout the application
  - **PRODUCT_OWNER** Used on the _About_ page
  - **IMG_PATH** absolute path to the root folder containing your images
  - **IMG_EXTENSION** file name extension of your images, case sensitive

```bash
export FLASK_APP=photobox
export PRODUCT_NAME="Foto Box"
export PRODUCT_OWNER="John Doe"
export IMG_PATH="/path/to/your/images"
export IMG_EXTENSION="jpg"

flask run
```

## Mounting a digital camera directly
Most users will want to present images captured my a digital camera "live" as
they are taken. This can be achieved by using the `gphoto2` FUSE file system
plugin and mounting the camera's memory card directly to some directory. The
directory can be found by looking through the output of `mount`.
