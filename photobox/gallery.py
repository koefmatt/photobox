import os
import glob

from PIL import Image
from flask import Blueprint, render_template, send_from_directory, send_file, current_app as app

bp = Blueprint('gallery', __name__)


@bp.route('/')
def index():
    ls = [os.path.basename(f) for f in glob.glob(app.config["IMG_PATH"] + "/**/*." + app.config["IMG_EXTENSION"],
                                                 recursive=True)]

    return render_template('gallery.html', images=ls, owner=app.config["PRODUCT_OWNER"],
                           product_name=app.config["PRODUCT_NAME"])


@bp.route('/img/<name>')
def image(name):
    return send_from_directory(app.config["IMG_PATH"], name)


@bp.route('/preview/<name>')
def preview(name):
    image_path = os.path.join(app.config["IMG_PATH"], name)
    thumbnail_path = os.path.join(app.config["IMG_PATH"], name + ".thumb")

    # create thumbnail if it does not already exist
    if not os.path.exists(thumbnail_path):
        size = 480, 480
        with Image.open(image_path) as im:
            im.thumbnail(size)
            im.save(thumbnail_path, "JPEG")

    return send_file(thumbnail_path)
