import os

from flask import Flask

from . import gallery


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        IMG_PATH=os.environ.get("IMG_PATH", default=app.instance_path),
        IMG_EXTENSION=os.environ.get("IMG_EXTENSION", default="JPG"),
        PRODUCT_NAME=os.environ.get("PRODUCT_NAME", default="Foto Box"),
        PRODUCT_OWNER=os.environ.get("PRODUCT_OWNER", default="Matthias Köferlein")
    )

    if test_config is None:
        app.config.from_pyfile('config.py', silent=True)
    else:
        app.config.from_mapping(test_config)

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    app.register_blueprint(gallery.bp)

    return app
